﻿using System;

namespace DbEngine
{
    public class QueryParser
    {
        private QueryParameter queryParameter;
        public QueryParser()
        {
            queryParameter = new QueryParameter();
        }

        /*
	 * this method will parse the queryString and will return the object of
	 * QueryParameter class
	 */

        public QueryParameter parseQuery(string queryString)
        {
           
           queryParameter.Fields = GetFields(queryString);
           queryParameter.FileName = GetFileName(queryString);
           queryParameter.LogicalOperators = GetLogicalOperators(queryString);
           queryParameter.GroupByFields = GetGroupByFields(queryString);
           queryParameter.OrderByFields = GetOrderByFields(queryString);
           queryParameter.Restrictions = GetRestrictions(queryString);
           queryParameter.AggregateFunctions = GetAggregateFunctions(queryString);
           return queryParameter;
        }

        private string GetFileName(string query)
        {
            string str = "";
            string str2 = "";
            for (int i = 0; i < query.Length; i++)
            {
                if (query[i] == '.')
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (query[j] == ' ')
                        {
                            break;
                        }
                        else
                        {
                            str += query[j];
                        }
                    }

                    for (int l = str.Length - 1; l >= 0; l--)
                    {
                        str2 += str[l];
                    }

                    str2 += '.';
                    for (int k = i + 1; k < query.Length; k++)
                    {
                        if (query[k] == ' ')
                        {
                            break;
                        }
                        else
                        {
                            str2 += query[k];
                        }
                    }
                }
            }
            return str2;
        }

        private string GetBaseQuery(string query)
        {
            string[] str = query.Split();
            string str2 = "";
            foreach (string str3 in str)
            {
                if (str3 == "where")
                {
                    break;
                }
                else
                {
                    str2 += str3 + " ";
                }
            }
            return str2.Trim();
        }

        private string GetConditionsPartQuery(string queryString)
        {
            string[] str = queryString.Split();
            string str2 = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != "where")
                {
                    str2 += str[i] + " ";
                }
                else
                {
                    if (str[i] == "where")
                    {
                        str2 += str[i];
                    }
                    else
                    {
                        break;
                    }
                    break;
                }
            }
            queryString = queryString.Replace(str2, "");
            return queryString.Trim();
        }

        private string[] GetConditions(string queryString)
        {
            string query = GetConditionsPartQuery(queryString);
            string[] str3 = query.Split();
            string[] str = new string[3];
            string s = "";
            if (query.Contains(" and ") && query.Contains(" or "))
            {
                int indx1 = Array.IndexOf(str3, "and");
                int indx2 = Array.IndexOf(str3, "or");
                if (indx1 < indx2)
                {
                    for (int i = 0; i < indx1; i++)
                    {
                        s += str3[i] + " ";
                    }
                    s += ',';
                    for (int j = indx1 + 1; j < indx2; j++)
                    {
                        s += str3[j] + " ";
                    }
                    s += ',';
                    for (int k = indx2 + 1; k < str3.Length; k++)
                    {
                        s += str3[k] + " ";
                    }
                }
                else
                {
                    for (int i = 0; i < indx2; i++)
                    {
                        s += str3[i] + " ";
                    }
                    s += ',';
                    for (int j = indx2 + 1; j < indx1; j++)
                    {
                        s += str3[j] + " ";
                    }
                    s += ',';
                    for (int k = indx1 + 1; k < str3.Length; k++)
                    {
                        s += str3[k] + " ";
                    }
                }
                str = s.Split(',');
            }
            else if (query.Contains("and"))
            {
                str = query.Split(" and ");
            }
            else if (query.Contains("or"))
            {
                str = query.Split(" or ");
            }
            else
            {
                str[0] = query;
            }

            return str;
        }

        /*
	 * extract the selected fields from the query string. Please note that we will
	 * need to extract the field(s) after "select" clause followed by a space from
	 * the query string. For eg: select city,win_by_runs from data/ipl.csv from the
	 * query mentioned above, we need to extract "city" and "win_by_runs". Please
	 * note that we might have a field containing name "from_date" or "from_hrs".
	 * Hence, consider this while parsing.
	 */
        private string[] GetFields(string queryString)
        {
            string[] str = queryString.Split();
            string str2 = "";
            foreach (string s in str)
            {
                if (s == "select")
                {
                    continue;
                }
                else if (s == "from")
                {
                    break;
                }
                else
                {
                    str2 += s;
                }
            }
            string[] str3 = str2.Split(',');
            return str3;
        }

        /*
	 * extract the conditions from the query string(if exists). for each condition,
	 * we need to capture the following: 1. Name of field 2. condition 3. value
	 * 
	 * For eg: select city,winner,team1,team2,player_of_match from data/ipl.csv
	 * where season >= 2008 or toss_decision != bat
	 * 
	 * here, for the first condition, "season>=2008" we need to capture: 1. Name of
	 * field: season 2. condition: >= 3. value: 2008 Also use trim() where ever
	 * required
	 * 
	 * the query might contain multiple conditions separated by OR/AND operators.
	 * Please consider this while parsing the conditions .
	 * 
	 */

        private FilterCondition[] GetRestrictions(string query)
        {
            
            string[] str = GetConditions(query);
            string[] ss = new string[3];
            string field = "";
            string val = "";
            string condition = "";
            
            if (str[0] != null)
            {
                string[] s = str[0].Split();
                field = s[0];
                val = s[2];
                condition = s[1];
                
            }
            else
            {
                Console.WriteLine("empty");
            }
            FilterCondition filterCondition = new FilterCondition(field,val,condition);
            FilterCondition[] filters = new FilterCondition[] { filterCondition };
            return filters;
        }

        /*
	 * extract the logical operators(AND/OR) from the query, if at all it is
	 * present. For eg: select city,winner,team1,team2,player_of_match from
	 * data/ipl.csv where season >= 2008 or toss_decision != bat and city =
	 * bangalore
	 * 
	 * the query mentioned above in the example should return a List of Strings
	 * containing [or,and]
	 */

        private string[] GetLogicalOperators(string queryString)
        {
            string[] str = queryString.Split();
                       
            if(queryString.Contains(" and ") && queryString.Contains(" or "))
            {
                string[] ans = new string[2];
                int j = 0;
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == "and" || str[i] == "or")
                    {
                        ans[j] = str[i];
                        j++;
                    }
                }
                return ans;
            }
            else if(queryString.Contains("and"))
            {
               for (int i = 0;i < str.Length; i++)
                {
                    if(str[i] == "and")
                    {
                        return new string[] { str[i] };
                    }
                }
            }
            else if (queryString.Contains(" or "))
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == "or")
                    {
                        return new string[] { str[i] };
                    }
                }
            }

            return null;
        }

        /*
             * extract the aggregate functions from the query. The presence of the aggregate
             * functions can determined if we have either "min" or "max" or "sum" or "count"
             * or "avg" followed by opening braces"(" after "select" clause in the query
             * string. in case it is present, then we will have to extract the same. For
             * each aggregate functions, we need to know the following: 1. type of aggregate
             * function(min/max/count/sum/avg) 2. field on which the aggregate function is
             * being applied
             * 
             * Please note that more than one aggregate function can be present in a query
             * 
             * 
             */
        private AggregateFunction[] GetAggregateFunctions(string queryString)
        {
            string field = "";
            string funct = "";
            if(queryString.Contains("count(") || queryString.Contains("sum(") || queryString.Contains("avg(") || queryString.Contains("min(") || queryString.Contains("max("))
            {
                string[] str = queryString.Split();
                if (str[1].Contains(","))
                {
                    string[] str1 = str[1].Split(',');
                    AggregateFunction[] aggregates = new AggregateFunction[str1.Length];
                    for (int i = 0; i < str1.Length; i++)
                    {
                        int indx = str1[i].IndexOf('(');
                        int indx2 = str1[i].IndexOf(')');
                        field = str1[i].Substring(indx + 1, indx2 - indx - 1);
                        funct = str1[i].Substring(0, indx);
                        aggregates[i] = new AggregateFunction(field, funct);
                    }
                    return aggregates;
                }
                else
                {
                    int indx = str[1].IndexOf('(');
                    int indx2 = str[1].IndexOf(')');
                    field = str[1].Substring(indx+1,indx2-indx-1);
                    funct = str[1].Substring(0, indx);
                    AggregateFunction aggregateFunction = new AggregateFunction(field, funct);
                    AggregateFunction[] aggregates = new AggregateFunction[] { aggregateFunction };
                    return aggregates;
                }
                
            }
            return null;
        }

        /*
	 * extract the order by fields from the query string. Please note that we will
	 * need to extract the field(s) after "order by" clause in the query, if at all
	 * the order by clause exists. For eg: select city,winner,team1,team2 from
	 * data/ipl.csv order by city from the query mentioned above, we need to extract
	 * "city". Please note that we can have more than one order by fields.
	 */
        private string[] GetOrderByFields(string queryString)
        {
            if(queryString.Contains("group by") && queryString.Contains("order by"))
            {
                int indx1 = queryString.IndexOf("order by");
                int indx2 = queryString.IndexOf("group by");
                string str = queryString.Substring(indx1+9);
                return new string[] { str };
            }
            else if (queryString.Contains("order by"))
            {
                int indx = queryString.IndexOf("by");
                string str = queryString.Substring(indx + 3);
                string[] str2 = str.Split();
                return str2;
            }
            else
            {
                return new string[] { "Order by Clause is not Present in the given Statement" };
            }
        }

        /*
	 * extract the group by fields from the query string. Please note that we will
	 * need to extract the field(s) after "group by" clause in the query, if at all
	 * the group by clause exists. For eg: select city,max(win_by_runs) from
	 * data/ipl.csv group by city from the query mentioned above, we need to extract
	 * "city". Please note that we can have more than one group by fields.
	 */
        private string[] GetGroupByFields(string queryString)
        {
            if (queryString.Contains("group by") && queryString.Contains("order by"))
            {
                int indx1 = queryString.IndexOf("order by");
                int indx2 = queryString.IndexOf("group by");
                string str = queryString.Substring(indx2 + 9,6);
                
                return new string[] {str};
            }
            else if (queryString.Contains("group"))
            {
                int indx = queryString.IndexOf("by");
                string str = queryString.Substring(indx + 3);
                string[] str2 = str.Split();
                return str2;
            }
            else
            {
                return new string[] { "Order by Clause is not Present in the given Statement" };
            }
        }
    }
}
