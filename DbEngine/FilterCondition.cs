﻿namespace DbEngine
{
    /*
 * This class is used for storing name of field, condition and value for 
 * each conditions
 * generate properties for this class,
 * Also override toString method
 * */
    public class FilterCondition
    {
        public string field;
        public string value;
        public string condition;
        // Write logic for constructor
        public FilterCondition(string propertyName,string propertyValue, string condition)
        {
            this.condition = condition;
            this.field = propertyName;
            this.value = propertyValue;
        }

    }
}
